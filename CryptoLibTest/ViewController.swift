//
//  ViewController.swift
//  CryptoLibTest
//
//  Created by Linxmap on 11/04/2015.
//  Copyright (c) 2015 Linxmap. All rights reserved.
//

import UIKit
import CryptoSwift

class ViewController: UIViewController {
    private let Salt:[UInt8] = [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
    private let iv = Cipher.randomIV(AES.blockSize)
    let userDefaults  = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /****************
        *CHACHA20
        ****************/
        //FIRST CASE ALWAYS WORKS, SAVE THE ENCRYPTED THEN READ THE ENCRYPTED SAVED NS DATA THEN DECRYPT THE USERNAME
        saveChaCha20()
        loadChaCha20()
        
        //SECOND CASE ALWAYS FAILS READ THE ENCRYPTED SAVED NS DATA THEN DECRYPT THE USERNAME
        loadChaCha20()
        
        
        /*******************
        *AES
        *******************/
        //FIRST CASE ALWAYS WORKS, SAVE THE ENCRYPTED THEN READ THE ENCRYPTED SAVED NS DATA THEN DECRYPT THE USERNAME
        saveAES()
        loadAES()
        
        //SECOND CASE ALWAYS FAILS READ THE ENCRYPTED SAVED NS DATA THEN DECRYPT THE USERNAME
        loadAES()
        
    }
   
    func saveChaCha20(){
    
        let username = "keran.fm@gmail.com"
        if let plainTextData:NSData = username.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false){
            if let encryptedChaCha20 = plainTextData.encrypt(Cipher.ChaCha20(key: Salt, iv: iv)){
                userDefaults.setObject(encryptedChaCha20, forKey: "data");
                println("SAVING encryptedChaCha20 : \(encryptedChaCha20)")
                userDefaults.synchronize()
            }
        }
        
        
    }
    
    func loadChaCha20(){
        
        if let  savedData = userDefaults.objectForKey("data") as! NSData!{
            println("reading from saved value : \(savedData) ")
            let decryptedSavedData = savedData.decrypt(Cipher.ChaCha20(key: Salt, iv: iv))
            let decryptedString:NSString = NSString(data: decryptedSavedData!, encoding: NSUTF8StringEncoding)!
            println("ChaCha20 - enc savedData : \(savedData)")
            println("ChaCha20 - decryptedSavedData : \(decryptedSavedData!)")
            println("ChaCha20 - decryptedString : \(decryptedString)")
            
        }
        else{
            println("data saved failed value nil ")
        }
        
    }

    func saveAES(){
        var username:String = "keran.fm@gmail.com"
        var usernameIntArray = [UInt8](username.utf8)
        let aes = AES(key: Salt, iv: iv, blockMode: CipherBlockMode.CBC)
        var returnData:[UInt8]! = aes?.encrypt(usernameIntArray, padding: PKCS7())
        println("saveAES returnData[UInt8] \(returnData)")
        let dataEncrypt:NSData = NSData.withBytes(returnData)
        userDefaults.setObject(dataEncrypt, forKey: "dataAES")
        userDefaults.synchronize()
        
        if let  data = userDefaults.objectForKey("dataAES") as! NSData!{
            println("dataAES saved success reading from saved value : \(data)" )
        }
        else{
            println("dataAES saved failed value nil ")
        }
    }
    
    func loadAES(){
        if let  data = userDefaults.objectForKey("dataAES") as! NSData!{
            
            //var encryptedIntArray = [UInt8](savedEncrypted.utf8)
            let pointer = UnsafePointer<UInt8>(data.bytes)
            let count = data.length
            // Get our buffer pointer and make an array out of it
            let buffer = UnsafeBufferPointer<UInt8>(start:pointer, count:count)
            let array = [UInt8](buffer)
            
            
            let decrypted = AES(key: Salt, iv: iv, blockMode: .CBC)?.decrypt(array, padding: PKCS7())
            let pointer1 = UnsafePointer<UInt8>(decrypted!)
            let returnDecrypted  = NSString(bytes: pointer1, length: decrypted!.count, encoding: NSUTF8StringEncoding)
            println("AES - decrypted user name : \(returnDecrypted)")
            
            println("dataAES saved success reading from saved value : \(data)" )
        }
        else{
            println("dataAES saved failed value nil ")
        }
        
    }
    

}

